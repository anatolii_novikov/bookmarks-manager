# Mono-repository for Bookmarks Manager application

Mono-repository is based on [Lerna](https://lerna.js.org/) + [Yarn Workspaces](https://classic.yarnpkg.com/en/docs/workspaces/)

Additional information about applications can be found in each own README.md files inside applications.

## Installation

### Clone repository

```bash
git clone https://anatolii_novikov@bitbucket.org/anatolii_novikov/bookmarks-manager.git
cd ./bookmarks-manager/
```

### Install dependencies

```bash
yarn install --force
```

## Supported commands

### Unit tests

```bash
yarn run test
```
