# Bookmarks Manager UI

## Overview

UI application for Bookmarks Manager. UI based on Angular 11+ and Angular material design.

Application has the next features:

* show a list of bookmarks grouped by field group
* show additional bookmark information on a separated page
* delete a bookmark
* add a new bookmark

UI has an adaptive design and can be used on mobile devices.

REST API is used to communicate with a server. 

### Main libraries
* [Angular 11+](https://angular.io/)
* [NgRx](https://ngrx.io/) is a state management
* [Angular Flex-Layout](https://github.com/angular/flex-layout)
* [Jest](https://jestjs.io/) is testing framework
* [jest-marbles](https://www.npmjs.com/package/jest-marbles) is matchers for RxJs marble testing

## Development process

### Running UI
A developer has two options to run the application:

```shell
 yarn run start
```
- Run UI without a backe-end mock server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

```shell
 yarn run start:local
```
- Run UI with a local back-end mock server. Mock supports REST API and allows you to get a bookmarks list, delete, and add bookmarks. There is additional information about mock server [json-server](https://github.com/typicode/json-server).

### Build

```shell
 yarn run build
```

The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Unit test is based on [Jest](https://jestjs.io/ru/)

```shell
 yarn run test
```

Execute the unit tests without a code coverage report.

```shell
 yarn run test:coverage
```

Generate a code coverage report. The report will be stored in the `coverage/` directory.

Current test coverage report:

File                   | % Stmts | % Branch | % Funcs | % Lines |
-----------------------|---------|----------|---------|---------|
All files              |    94.5 |     87.5 |   83.33 |   93.82 |

### Code quality

```shell
 yarn run lint
```

The command runs [ESLint](https://eslint.org/) static code analyzer for `*.ts` files and runs [stylelint](https://stylelint.io/) for `*.scss` files.

```shell
 yarn run format
```

The command executes [Prettier](https://prettier.io/) to format code for `*.ts`, `*.html` and `*.scss` files.

## UI examples

Bookmarks list page

![Bookmarks list page](docs/readme-images/list.jpg)

Bookmarks list page mobile version

![Bookmarks list page](docs/readme-images/list-page-mobile-view.jpg)

Add page

![Add page](docs/readme-images/add-page.jpg)

Add page with validation errors

![Add page](docs/readme-images/add-page-validation-errors.jpg)

Bookmark info page

![Add page](docs/readme-images/info-page.jpg)
