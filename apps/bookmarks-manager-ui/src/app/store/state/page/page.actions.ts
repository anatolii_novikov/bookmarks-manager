import { Action } from '@ngrx/store';

export enum PageActionsEnum {
  taskLoadingStarted = '[Page] Task Loading Started',
  taskLoadedSuccessfully = '[Page] Task Loaded Successfully',
  pageLoadedSuccessfully = '[Page] Page Loaded Successfully',
}

export class TaskLoadingStarted implements Action {
  public readonly type = PageActionsEnum.taskLoadingStarted;
}

export class TaskLoadedSuccessfully implements Action {
  public readonly type = PageActionsEnum.taskLoadedSuccessfully;
}

export class PageLoadedSuccessfully implements Action {
  public readonly type = PageActionsEnum.pageLoadedSuccessfully;
}
