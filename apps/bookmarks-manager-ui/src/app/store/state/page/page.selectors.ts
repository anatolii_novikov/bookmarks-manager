import { createSelector } from '@ngrx/store';

import { AppState } from '../types';
import { PageState } from './types';

const selectPageFromAppState = (state: AppState) => state.page;

export const selectIsLoading = createSelector(
  selectPageFromAppState,
  (state: PageState) => state.isLoading,
);
