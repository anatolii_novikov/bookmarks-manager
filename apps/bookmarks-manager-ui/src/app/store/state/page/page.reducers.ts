import { PageActions, PageState } from './types';
import { PageActionsEnum } from './page.actions';
import { initialPageState } from './page.state';

let loadingTasksCount = 0;

export const pageReducers = (
  state = initialPageState,
  action: PageActions,
): PageState => {
  switch (action.type) {
    case PageActionsEnum.pageLoadedSuccessfully: {
      loadingTasksCount = 0;
      return {
        ...state,
        isLoading: false,
      };
    }

    case PageActionsEnum.taskLoadingStarted: {
      loadingTasksCount++;
      return {
        ...state,
        isLoading: true,
      };
    }

    case PageActionsEnum.taskLoadedSuccessfully: {
      loadingTasksCount--;
      if (loadingTasksCount < 0) {
        loadingTasksCount = 0;
      }

      return loadingTasksCount === 0
        ? {
            ...state,
            isLoading: false,
          }
        : state;
    }

    default:
      return state;
  }
};
