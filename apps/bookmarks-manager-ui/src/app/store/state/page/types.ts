import {
  PageLoadedSuccessfully,
  TaskLoadedSuccessfully,
  TaskLoadingStarted,
} from './page.actions';

export interface PageState {
  isLoading: boolean;
}

export type PageActions =
  | TaskLoadingStarted
  | TaskLoadedSuccessfully
  | PageLoadedSuccessfully;
