import { PageState } from './types';

export const initialPageState: PageState = {
  isLoading: false,
};
