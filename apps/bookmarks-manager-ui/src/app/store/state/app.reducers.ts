import { ActionReducerMap } from '@ngrx/store';

import { AppState } from './types';
import { PageActions } from './page/types';
import { pageReducers } from './page/page.reducers';

export const appReducers: ActionReducerMap<AppState, PageActions> = {
  page: pageReducers,
};
