import { PageState } from './page/types';

export interface AppState {
  page: PageState;
}
