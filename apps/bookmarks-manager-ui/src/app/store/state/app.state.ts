import { AppState } from './types';
import { initialPageState } from './page/page.state';

export const initialAppState: AppState = {
  page: initialPageState,
};
