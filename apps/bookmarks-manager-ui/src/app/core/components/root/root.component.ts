import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';

import { AppState, selectIsLoading } from '@app/store';

@Component({
  selector: 'bookmarks-manager-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.scss'],
})
export class RootComponent {
  public isLoading$: Observable<boolean> = this.store.pipe(
    select(selectIsLoading),
  );

  constructor(private store: Store<AppState>) {}
}
