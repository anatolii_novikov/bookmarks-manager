import { TestBed, ComponentFixture } from '@angular/core/testing';
import { Component } from '@angular/core';
import { provideMockStore, MockStore } from '@ngrx/store/testing';

import { RootComponent } from './root.component';

@Component({ selector: 'router-outlet', template: '' })
class RouterOutletStubComponent {}

@Component({
  selector: 'mat-spinner',
  template: '<ng-content></ng-content>',
})
class MatSpinnerStubComponent {}

describe('MainComponent', () => {
  const initialState = { page: { isLoading: false } };

  let store: MockStore;
  let fixture: ComponentFixture<RootComponent>;
  let mainTemplate: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        RouterOutletStubComponent,
        MatSpinnerStubComponent,
        RootComponent,
      ],
      providers: [provideMockStore({ initialState })],
    }).compileComponents();

    fixture = TestBed.createComponent(RootComponent);
    store = TestBed.inject(MockStore);
    mainTemplate = fixture.debugElement.nativeElement;
  });

  describe('creating an instance', () => {
    it('should create the component', () => {
      const app = fixture.debugElement.componentInstance;
      expect(app).toBeTruthy();
    });

    it('should create an instance of RootComponent class', () => {
      const app = fixture.debugElement.componentInstance;
      expect(app instanceof RootComponent).toBe(true);
    });
  });

  describe('checking a template', () => {
    describe('show/hide loader workflow', () => {
      it('should show & hide the loader depends on page loading status', () => {
        store.setState({ page: { isLoading: true } });
        fixture.detectChanges();

        let testedComponent;

        testedComponent = mainTemplate.querySelector('mat-spinner');
        expect(testedComponent).toBeTruthy();

        store.setState({ page: { isLoading: false } });
        fixture.detectChanges();

        testedComponent = mainTemplate.querySelector('mat-spinner');
        expect(testedComponent).toBeFalsy();
      });

      it('should show & hide routing component depends on page loading status', () => {
        store.setState({ page: { isLoading: true } });
        fixture.detectChanges();

        let routingComponent;
        routingComponent = mainTemplate.querySelector('router-outlet');
        expect(routingComponent).toBeFalsy();

        store.setState({ page: { isLoading: false } });
        fixture.detectChanges();

        routingComponent = mainTemplate.querySelector('router-outlet');
        expect(routingComponent).toBeTruthy();
      });
    });
  });
});
