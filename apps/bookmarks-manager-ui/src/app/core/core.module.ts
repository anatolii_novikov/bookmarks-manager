import { BrowserModule } from '@angular/platform-browser';
import {
  NoopAnimationsModule,
  BrowserAnimationsModule,
} from '@angular/platform-browser/animations';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FlexLayoutModule } from '@angular/flex-layout';

import { RoutingModule } from '@app/modules/core/routing/routing.module';
import { ApiModule } from '@app/modules/core/api/api.module';
import { StoreModule } from '@app/store/store.module';

import { RootComponent } from './components/root/root.component';

@NgModule({
  declarations: [RootComponent],
  imports: [
    BrowserModule,
    NoopAnimationsModule,
    BrowserAnimationsModule,
    ApiModule,
    StoreModule,
    RoutingModule,
    MatProgressSpinnerModule,
    FlexLayoutModule,
  ],
  bootstrap: [RootComponent],
})
export class CoreModule {
  constructor(
    @Optional()
    @SkipSelf()
    parentModule: CoreModule,
  ) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded.');
    }
  }
}
