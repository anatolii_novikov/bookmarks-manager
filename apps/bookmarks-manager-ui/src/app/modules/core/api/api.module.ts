import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { environment } from '@env/environment';
import { BookmarksApiService } from './index';
import { API_BOOKMARKS_SERVICE_URL } from './constants';

@NgModule({
  imports: [HttpClientModule],
  providers: [
    {
      provide: API_BOOKMARKS_SERVICE_URL,
      useValue: new URL(
        environment.api.bookmarksApi.pathUrl,
        environment.api.baseUrl,
      ),
    },
    BookmarksApiService,
  ],
})
export class ApiModule {}
