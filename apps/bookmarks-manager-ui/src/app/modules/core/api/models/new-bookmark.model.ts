export interface NewBookmark {
  name: string;
  url: string;
  group: string;
}
