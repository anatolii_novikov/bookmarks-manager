import { InjectionToken } from '@angular/core';

export const API_BOOKMARKS_SERVICE_URL = new InjectionToken<URL>(
  'API_BOOKMARKS_SERVICE_URL',
);
