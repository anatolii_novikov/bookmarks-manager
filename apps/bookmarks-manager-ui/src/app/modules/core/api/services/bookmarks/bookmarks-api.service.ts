import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { API_BOOKMARKS_SERVICE_URL } from '../../constants';
import { Bookmark } from '../../models/bookmark.model';
import { NewBookmark } from '../../models/new-bookmark.model';

@Injectable()
export class BookmarksApiService {
  private bookmarksUrlPath = 'bookmarks';

  constructor(
    @Inject(HttpClient) private http: HttpClient,
    @Inject(API_BOOKMARKS_SERVICE_URL) private apiBookmarksServiceUrl: URL,
  ) {}

  getBookmarksList(): Observable<Bookmark[]> {
    const getBookmarksUrl = `${this.apiBookmarksServiceUrl}${this.bookmarksUrlPath}`;
    return this.http.get<Bookmark[]>(getBookmarksUrl);
  }

  getBookmark(bookmarkId: string): Observable<Bookmark> {
    const getBookmarkUrl = `${this.apiBookmarksServiceUrl}${this.bookmarksUrlPath}/${bookmarkId}`;
    return this.http.get<Bookmark>(getBookmarkUrl);
  }

  deleteBookmark(bookmarkId: string): Observable<true> {
    const getBookmarkUrl = `${this.apiBookmarksServiceUrl}${this.bookmarksUrlPath}/${bookmarkId}`;
    return this.http.delete<true>(getBookmarkUrl);
  }

  addBookmark(bookmark: NewBookmark): Observable<Bookmark> {
    const getBookmarkUrl = `${this.apiBookmarksServiceUrl}${this.bookmarksUrlPath}`;
    return this.http.post<Bookmark>(getBookmarkUrl, bookmark);
  }
}
