import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PageNotFoundModule } from '@app/modules/core/page-not-found/page-not-found.module';
import { LayoutModule } from '@app/modules/core/layout/layout.module';
import { routes } from './routes';

@NgModule({
  imports: [LayoutModule, PageNotFoundModule, RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class RoutingModule {}
