import { Routes } from '@angular/router';

import { PageNotFoundComponent } from '@app/modules/core/page-not-found/components/page-not-found.component';
import { LayoutComponent } from '@app/modules/core/layout/components/layout/layout.component';
import { URL_PATH_PAGE_NOT_FOUND, URL_PATH_BOOKMARKS } from './constants';

export const routes: Routes = [
  { path: '', redirectTo: `/${URL_PATH_BOOKMARKS}`, pathMatch: 'full' },

  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: URL_PATH_BOOKMARKS,
        loadChildren: () =>
          import('@app/bookmarks/bookmarks.module').then(
            module => module.BookmarksModule,
          ),
      },
    ],
  },

  { path: URL_PATH_PAGE_NOT_FOUND, component: PageNotFoundComponent },
  { path: '**', component: PageNotFoundComponent },
];
