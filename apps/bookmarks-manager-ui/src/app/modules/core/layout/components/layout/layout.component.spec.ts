import { TestBed } from '@angular/core/testing';
import { Component } from '@angular/core';

import { SharedModule } from '@app/shared/__mocks__/shared.module';
import { LayoutComponent as TestedComponent } from './layout.component';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'router-outlet',
  template: '<ng-content></ng-content>',
})
class RouterOutletStubComponent {}

describe('NotLoggedLayoutComponent', () => {
  let fixture;
  let mainTemplate: HTMLElement;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [RouterOutletStubComponent, TestedComponent],
    }).compileComponents();
  });

  describe('creating an instance', () => {
    let app: TestedComponent;
    beforeEach(() => {
      fixture = TestBed.createComponent(TestedComponent);
      app = fixture.debugElement.componentInstance;
    });
    it('should create the component', () => {
      expect(app).toBeTruthy();
    });

    it('should create an instance of NotLoggedLayoutComponent class', () => {
      expect(app).toBeInstanceOf(TestedComponent);
    });
  });

  describe('checking a template', () => {
    beforeEach(() => {
      fixture = TestBed.createComponent(TestedComponent);
      mainTemplate = fixture.debugElement.nativeElement;
    });

    it('should contain a header', () => {
      const testedElement = mainTemplate.querySelector('header');
      expect(testedElement).toBeTruthy();
    });

    it('should contain main content', () => {
      const testedElement = mainTemplate.querySelector('main');
      expect(testedElement).toBeTruthy();
    });

    it('should contain footer', () => {
      const testedElement = mainTemplate.querySelector('footer');
      expect(testedElement).toBeTruthy();
    });
  });
});
