import { Component } from '@angular/core';
import { TestBed } from '@angular/core/testing';

import { HeaderComponent as TestedComponent } from './header.component';

@Component({
  selector: 'mat-toolbar',
  template: '<ng-content></ng-content>',
})
class MatToolbarStubComponent {}

describe('HeaderComponent', () => {
  let fixture;
  let mainTemplate: HTMLElement;

  beforeEach(() => {
    jest.clearAllMocks();

    TestBed.configureTestingModule({
      declarations: [MatToolbarStubComponent, TestedComponent],
    }).compileComponents();
  });

  describe('creating an instance', () => {
    let app: TestedComponent;
    beforeEach(() => {
      fixture = TestBed.createComponent(TestedComponent);
      app = fixture.debugElement.componentInstance;
    });
    it('should create the component', () => {
      expect(app).toBeTruthy();
    });

    it('should create an instance of HeaderComponent class', () => {
      expect(app).toBeInstanceOf(TestedComponent);
    });
  });
});
