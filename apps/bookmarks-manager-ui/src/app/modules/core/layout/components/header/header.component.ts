import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'bookmarks-manager-header',
  templateUrl: './header.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {}
