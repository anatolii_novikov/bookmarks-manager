import { TestBed, async, ComponentFixture } from '@angular/core/testing';

import { FooterComponent as TestedComponent } from './footer.component';

describe('FooterComponent', () => {
  let fixture: ComponentFixture<TestedComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestedComponent],
    }).compileComponents();
    fixture = TestBed.createComponent(TestedComponent);
  }));

  describe('creating an instance', () => {
    let app: TestedComponent;
    beforeEach(async(() => {
      app = fixture.debugElement.componentInstance;
    }));
    it('should create the component', () => {
      expect(app).toBeTruthy();
    });

    it('should create an instance of FooterComponent class', () => {
      expect(app).toBeInstanceOf(TestedComponent);
    });
  });

  describe('checking a template', () => {
    it('should contain a footer title', () => {
      const mainTemplate = fixture.debugElement.nativeElement;
      const testedElement = mainTemplate.querySelector('p');
      expect(testedElement).toBeTruthy();
      expect(testedElement.textContent).toBe('© Bookmarks Manager');
    });
  });
});
