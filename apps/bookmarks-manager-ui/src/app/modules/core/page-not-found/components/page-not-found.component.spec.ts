import { TestBed, async } from '@angular/core/testing';

import { PageNotFoundComponent as TestedComponent } from './page-not-found.component';

describe('PageNotFoundComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestedComponent],
    }).compileComponents();
  }));

  describe('creating an instance', () => {
    it('should create the component', () => {
      const fixture = TestBed.createComponent(TestedComponent);
      const app = fixture.debugElement.componentInstance;
      expect(app).toBeTruthy();
    });

    it('should create an instance of PageNotFoundComponent class', () => {
      const fixture = TestBed.createComponent(TestedComponent);
      const app = fixture.debugElement.componentInstance;
      expect(app instanceof TestedComponent).toBe(true);
    });
  });

  describe('checking a template', () => {
    it('should contain a title', () => {
      const fixture = TestBed.createComponent(TestedComponent);
      const mainTemplate = fixture.debugElement.nativeElement;
      const h2Element = mainTemplate.querySelector('h2');
      expect(h2Element).toBeTruthy();
      expect(h2Element.textContent).toBe('Page not found');
    });
  });
});
