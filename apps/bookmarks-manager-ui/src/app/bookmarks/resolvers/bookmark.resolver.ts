import { Injectable } from '@angular/core';
import { select } from '@ngrx/store';

import { Bookmark } from '@api/models/bookmark.model';
import { selectSelectedBookmark } from '../state/bookmarks/bookmarks.selectors';
import { BookmarkRequested } from '../state/bookmarks/bookmarks.actions';
import { EntityAbstractResolver } from './abstract/entity-abstract.resolver';

@Injectable()
export class BookmarkResolver extends EntityAbstractResolver<Bookmark | false> {
  public checkedEntity$ = this.store.pipe(select(selectSelectedBookmark));

  protected loadEntityById(id: string) {
    this.store.dispatch(new BookmarkRequested(id));
  }
}
