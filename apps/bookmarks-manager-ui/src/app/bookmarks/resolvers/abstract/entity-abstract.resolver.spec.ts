import { fakeAsync, tick, TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { cold, hot, Scheduler } from 'jest-marbles';
import { provideMockStore, MockStore } from '@ngrx/store/testing';

import { TaskLoadedSuccessfully, TaskLoadingStarted } from '@app/store';
import SpyInstance = jest.SpyInstance;
import { EntityAbstractResolver } from './entity-abstract.resolver';

interface StubStreamEntity {
  test: string;
}

@Injectable()
export class StubResolver extends EntityAbstractResolver<StubStreamEntity> {
  public checkedEntity$!: Observable<StubStreamEntity>;
}

describe('EntityAbstractResolver', () => {
  const stubData: StubStreamEntity = {
    test: 'test string',
  };
  const initialState = {};
  const routeMock = { params: {} };

  let resolver: StubResolver;
  let store: MockStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({ initialState }), StubResolver],
    });
    resolver = TestBed.inject(StubResolver);
    store = TestBed.inject(MockStore);
  });

  describe('creating an instance', () => {
    it('should create the resolver', () => {
      expect(resolver).toBeTruthy();
      expect(resolver).toBeInstanceOf(EntityAbstractResolver);
    });
  });

  describe('resolve()', () => {
    let dispatchSpy: SpyInstance;
    let loadEntitySpy: SpyInstance;

    beforeEach(() => {
      dispatchSpy = jest.spyOn(store, 'dispatch');
      // @ts-expect-error
      loadEntitySpy = jest.spyOn(resolver, 'loadEntity');
    });

    it('should return Observable<true> if entity exists', () => {
      resolver.checkedEntity$ = hot('--d--|', { d: stubData });
      const expectedResult = cold('--(r|)', { r: true });

      // @ts-ignore
      const result = resolver.resolve(null, null);
      expect(result).toBeObservable(expectedResult);
    });

    it('should get only first success result', () => {
      resolver.checkedEntity$ = hot('--d-d-|', { d: stubData });
      const expectedResult = cold('--(r|)', { r: true });

      // @ts-ignore
      const result = resolver.resolve(routeMock, null);
      expect(result).toBeObservable(expectedResult);
    });

    it('should ignore entity is null', () => {
      resolver.checkedEntity$ = hot('--n--d--|', { n: null, d: stubData });

      const expectedResult = cold('-----(r|)', { r: true });

      // @ts-ignore
      const result = resolver.resolve(routeMock, null);
      expect(result).toBeObservable(expectedResult);
    });

    it('should ignore entity is undefined', () => {
      resolver.checkedEntity$ = hot('--n--d--|', { n: undefined, d: stubData });

      const expectedResult = cold('-----(r|)', { r: true });

      // @ts-ignore
      const result = resolver.resolve(routeMock, null);
      expect(result).toBeObservable(expectedResult);
    });

    it('should dispatch an event TaskLoadingStarted', done => {
      resolver.checkedEntity$ = hot('-n-d--|', { n: null, d: stubData });

      expect.assertions(1);
      // @ts-ignore
      resolver.resolve(routeMock, null).subscribe(() => {
        expect(dispatchSpy.mock.calls[0][0]).toEqual(
          expect.any(TaskLoadingStarted),
        );
        done();
      });

      Scheduler.get().flush();
    });

    it('should dispatch an event TaskLoadingStarted only once', done => {
      resolver.checkedEntity$ = hot('-n-e-d--|', {
        n: null,
        e: null,
        d: stubData,
      });

      expect.assertions(1);
      // @ts-ignore
      resolver.resolve(routeMock, null).subscribe(() => {
        expect(dispatchSpy).toHaveBeenCalledTimes(1);
        done();
      });

      Scheduler.get().flush();
    });

    it('should load the checked entity and only once', done => {
      resolver.checkedEntity$ = hot('-n-e-d--|', {
        n: null,
        e: null,
        d: stubData,
      });

      expect.assertions(1);
      // @ts-ignore
      resolver.resolve(routeMock, null).subscribe(() => {
        expect(loadEntitySpy).toHaveBeenCalledTimes(1);
        done();
      });

      Scheduler.get().flush();
    });

    it('should dispatch an event TaskLoadedSuccessfully after an entity was loaded', fakeAsync(() => {
      resolver.checkedEntity$ = hot('-n-d--|', { n: null, d: stubData });

      // @ts-ignore
      resolver.resolve(routeMock, null).subscribe(
        () => {},
        () => {},
        () => {},
      );

      Scheduler.get().flush();
      tick();

      expect.assertions(1);

      expect(dispatchSpy.mock.calls[1][0]).toEqual(
        expect.any(TaskLoadedSuccessfully),
      );
    }));
  });
});
