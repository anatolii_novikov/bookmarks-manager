import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs';
import { tap, map, filter, first, finalize } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import {
  TaskLoadingStarted,
  TaskLoadedSuccessfully,
  AppState,
} from '@app/store';

@Injectable()
export class EntityAbstractResolver<T> implements Resolve<true> {
  public checkedEntity$!: Observable<T | null>;

  constructor(protected store: Store<AppState>) {}

  resolve(
    route: ActivatedRouteSnapshot,
    _: RouterStateSnapshot,
  ): Observable<true> {
    let isPageLoadingStarted = false;
    return this.checkedEntity$.pipe(
      tap(checkedEntity => {
        if (checkedEntity === null && !isPageLoadingStarted) {
          this.pageLoadingStarted();
          if (typeof route.params['id'] === 'string') {
            // TODO: Add logic for id validation
            this.loadEntityById(route.params['id']);
          } else {
            this.loadEntity();
          }
          isPageLoadingStarted = true;
        }
      }),
      filter(
        checkedEntity => checkedEntity !== null && checkedEntity !== undefined,
      ),
      map(checkedEntity => true as const),
      first<true>(),
      finalize(() => this.pageLoadedSuccessfully()),
    );
  }

  protected loadEntity() {}

  protected loadEntityById(id: string) {}

  private pageLoadingStarted() {
    this.store.dispatch(new TaskLoadingStarted());
  }

  private pageLoadedSuccessfully() {
    this.store.dispatch(new TaskLoadedSuccessfully());
  }
}
