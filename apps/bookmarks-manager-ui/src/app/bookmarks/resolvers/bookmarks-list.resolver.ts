import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { select } from '@ngrx/store';

import { Bookmark } from '@api/models/bookmark.model';
import { selectBookmarksList } from '../state/bookmarks/bookmarks.selectors';
import { BookmarksListRequested } from '../state/bookmarks/bookmarks.actions';
import { EntityAbstractResolver } from './abstract/entity-abstract.resolver';

@Injectable()
export class BookmarksListResolver extends EntityAbstractResolver<Bookmark[]> {
  public checkedEntity$: Observable<Bookmark[] | null> = this.store.pipe(
    select(selectBookmarksList),
  );

  protected loadEntity() {
    this.store.dispatch(new BookmarksListRequested());
  }
}
