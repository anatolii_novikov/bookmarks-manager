import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatListModule } from '@angular/material/list';
import { MatExpansionModule } from '@angular/material/expansion';
import { StoreModule as StoreModuleNgrx } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { OrderModule } from 'ngx-order-pipe';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

import { SharedModule } from '@app/shared/shared.module';
import { routes } from './routes';
import { moduleReducers } from './state/module.reducers';
import { STORE_FEATURE_KEY } from './constants';
import { BookmarksListResolver } from './resolvers/bookmarks-list.resolver';
import { BookmarksEffects } from './state/bookmarks/bookmarks.effects';
import { SelectedBookmarkComponent } from './components/selected-bookmark/selected-bookmark.component';
import { AddBookmarkComponent } from './components/add-bookmark/add-bookmark.component';
import { BookmarksComponent } from './components/bookmarks/bookmarks.component';
import { BookmarkResolver } from './resolvers/bookmark.resolver';

@NgModule({
  imports: [
    SharedModule,
    MatListModule,
    MatCardModule,
    MatInputModule,
    FormsModule,
    OrderModule,
    MatAutocompleteModule,
    StoreModuleNgrx.forFeature(STORE_FEATURE_KEY, moduleReducers),
    EffectsModule.forFeature([BookmarksEffects]),
    RouterModule.forChild(routes),
    MatExpansionModule,
  ],
  declarations: [
    BookmarksComponent,
    SelectedBookmarkComponent,
    AddBookmarkComponent,
  ],
  providers: [BookmarksListResolver, BookmarkResolver],
})
export class BookmarksModule {}
