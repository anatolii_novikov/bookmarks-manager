import { ModuleState } from './types';
import { initialBookmarksState } from './bookmarks/bookmarks.state';

export const initialModuleState: ModuleState = {
  bookmarks: initialBookmarksState,
};
