import { createSelector, createFeatureSelector } from '@ngrx/store';

import { STORE_FEATURE_KEY } from '../../constants';
import { ModuleState } from '../types';

const selectModuleStateFromAppState = createFeatureSelector<ModuleState>(
  STORE_FEATURE_KEY,
);

export const selectBookmarksList = createSelector(
  selectModuleStateFromAppState,
  (state: ModuleState) => state.bookmarks.bookmarksList,
);

export const selectSelectedBookmark = createSelector(
  selectModuleStateFromAppState,
  (state: ModuleState) => state.bookmarks.selectedBookmark,
);
