import { Action } from '@ngrx/store';

import { Bookmark } from '@api/models/bookmark.model';
import { NewBookmark } from '@api/models';

export enum BookmarksActionsEnum {
  bookmarksListRequested = '[Bookmarks] Bookmarks List Requested',
  bookmarksListLoadedSuccessfully = '[Bookmarks] Bookmarks List Loaded Successfully',
  bookmarkRequested = '[Bookmarks] Bookmark Requested',
  bookmarkLoadedSuccessfully = '[Bookmarks] Bookmark Loaded Successfully',
  bookmarkLoadedErrorOccurred = '[Bookmarks] Bookmark Loaded Error Occurred',
  bookmarkDeleted = '[Bookmarks] Bookmark Deleted',
  bookmarkDeletedSuccessfully = '[Bookmarks] Bookmark Deleted Successfully',
  bookmarkAdded = '[Bookmarks] Bookmark Added',
  bookmarkAddedSuccessfully = '[Bookmarks] Bookmark Added Successfully',
}

export class BookmarksListRequested implements Action {
  public readonly type = BookmarksActionsEnum.bookmarksListRequested;
}

export class BookmarksListLoadedSuccessfully implements Action {
  public readonly type = BookmarksActionsEnum.bookmarksListLoadedSuccessfully;

  constructor(public payload: Bookmark[]) {}
}

export class BookmarkRequested implements Action {
  public readonly type = BookmarksActionsEnum.bookmarkRequested;

  constructor(public payload: string) {}
}

export class BookmarkLoadedSuccessfully implements Action {
  public readonly type = BookmarksActionsEnum.bookmarkLoadedSuccessfully;

  constructor(public payload: Bookmark) {}
}

export class BookmarkLoadedErrorOccurred implements Action {
  public readonly type = BookmarksActionsEnum.bookmarkLoadedErrorOccurred;
}

export class BookmarkDeleted implements Action {
  public readonly type = BookmarksActionsEnum.bookmarkDeleted;

  constructor(public payload: string) {}
}

export class BookmarkDeletedSuccessfully implements Action {
  public readonly type = BookmarksActionsEnum.bookmarkDeletedSuccessfully;
}

export class BookmarkAdded implements Action {
  public readonly type = BookmarksActionsEnum.bookmarkAdded;

  constructor(public payload: NewBookmark) {}
}

export class BookmarkAddedSuccessfully implements Action {
  public readonly type = BookmarksActionsEnum.bookmarkAddedSuccessfully;
}
