import { Inject, Injectable } from '@angular/core';
import { ofType, Actions, createEffect } from '@ngrx/effects';
import { map, switchMap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

import { BookmarksApiService } from '@api/index';
import { Bookmark } from '@api/models';
import {
  BookmarksActionsEnum,
  BookmarksListLoadedSuccessfully,
  BookmarksListRequested,
  BookmarkRequested,
  BookmarkLoadedSuccessfully,
  BookmarkLoadedErrorOccurred,
  BookmarkDeletedSuccessfully,
  BookmarkDeleted,
  BookmarkAdded,
  BookmarkAddedSuccessfully,
} from './bookmarks.actions';

@Injectable()
export class BookmarksEffects {
  public loadBookmarksList$ = createEffect(() =>
    this.actions$.pipe(
      ofType<BookmarksListRequested>(
        BookmarksActionsEnum.bookmarksListRequested,
      ),
      switchMap(() => this.bookmarksApiService.getBookmarksList()),
      map(
        (bookmarksList: Bookmark[]) =>
          new BookmarksListLoadedSuccessfully(bookmarksList),
      ),
    ),
  );

  public loadBookmark$ = createEffect(() =>
    this.actions$.pipe(
      ofType<BookmarkRequested>(BookmarksActionsEnum.bookmarkRequested),
      switchMap(({ payload: bookmarkId }) =>
        this.bookmarksApiService.getBookmark(bookmarkId),
      ),
      map((bookmark: Bookmark) => new BookmarkLoadedSuccessfully(bookmark)),
      catchError(() => of(new BookmarkLoadedErrorOccurred())),
    ),
  );

  public deleteBookmark$ = createEffect(() =>
    this.actions$.pipe(
      ofType<BookmarkDeleted>(BookmarksActionsEnum.bookmarkDeleted),
      switchMap(({ payload: bookmarkId }) =>
        this.bookmarksApiService.deleteBookmark(bookmarkId),
      ),
      map(() => new BookmarkDeletedSuccessfully()),
    ),
  );

  public addBookmark$ = createEffect(() =>
    this.actions$.pipe(
      ofType<BookmarkAdded>(BookmarksActionsEnum.bookmarkAdded),
      switchMap(({ payload: bookmark }) =>
        this.bookmarksApiService.addBookmark(bookmark),
      ),
      map(() => new BookmarkAddedSuccessfully()),
    ),
  );

  constructor(
    @Inject(BookmarksApiService)
    private bookmarksApiService: BookmarksApiService,
    @Inject(Actions) private actions$: Actions,
  ) {}
}
