import { Bookmark } from '@api/models/bookmark.model';
import {
  BookmarksListLoadedSuccessfully,
  BookmarksListRequested,
  BookmarkRequested,
  BookmarkLoadedSuccessfully,
  BookmarkLoadedErrorOccurred,
  BookmarkDeletedSuccessfully,
  BookmarkAdded,
  BookmarkAddedSuccessfully,
} from './bookmarks.actions';

export interface BookmarksState {
  bookmarksList: null | Bookmark[];
  selectedBookmark: null | Bookmark | false;
}

export type BookmarksActions =
  | BookmarksListLoadedSuccessfully
  | BookmarksListRequested
  | BookmarkRequested
  | BookmarkLoadedSuccessfully
  | BookmarkLoadedErrorOccurred
  | BookmarkDeletedSuccessfully
  | BookmarkAdded
  | BookmarkAddedSuccessfully;
