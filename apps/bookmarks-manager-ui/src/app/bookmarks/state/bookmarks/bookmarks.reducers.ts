import { BookmarksActions, BookmarksState } from './types';
import { BookmarksActionsEnum } from './bookmarks.actions';
import { initialBookmarksState } from './bookmarks.state';

export const bookmarksReducers = (
  state = initialBookmarksState,
  action: BookmarksActions,
): BookmarksState => {
  switch (action.type) {
    case BookmarksActionsEnum.bookmarksListLoadedSuccessfully: {
      return {
        ...state,
        bookmarksList: action.payload,
      };
    }

    case BookmarksActionsEnum.bookmarkLoadedSuccessfully: {
      return {
        ...state,
        selectedBookmark: action.payload,
      };
    }

    case BookmarksActionsEnum.bookmarkLoadedErrorOccurred: {
      return {
        ...state,
        selectedBookmark: false,
      };
    }

    case BookmarksActionsEnum.bookmarkDeletedSuccessfully: {
      return {
        ...state,
        ...initialBookmarksState,
      };
    }

    case BookmarksActionsEnum.bookmarkAddedSuccessfully: {
      return {
        ...state,
        ...initialBookmarksState,
      };
    }

    default:
      return state;
  }
};
