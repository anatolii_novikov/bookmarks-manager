import { BookmarksState } from './types';

export const initialBookmarksState: BookmarksState = {
  bookmarksList: null,
  selectedBookmark: null,
};
