import { BookmarksState } from './bookmarks/types';

export interface ModuleState {
  bookmarks: BookmarksState;
}
