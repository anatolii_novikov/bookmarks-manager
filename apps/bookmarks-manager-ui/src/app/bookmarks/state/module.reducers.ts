import { ActionReducerMap } from '@ngrx/store';

import { ModuleState } from './types';
import { BookmarksActions } from './bookmarks/types';
import { bookmarksReducers } from './bookmarks/bookmarks.reducers';

export const moduleReducers: ActionReducerMap<ModuleState, BookmarksActions> = {
  bookmarks: bookmarksReducers,
};
