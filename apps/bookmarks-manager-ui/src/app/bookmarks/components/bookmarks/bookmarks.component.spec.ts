import {
  TestBed,
  ComponentFixture,
  fakeAsync,
  tick,
} from '@angular/core/testing';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Component, NgZone, ViewChild } from '@angular/core';
import { Router, RouterOutlet } from '@angular/router';
import { Location } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { OrderModule } from 'ngx-order-pipe';

import {
  RouterStubComponent,
  SharedModule,
} from '@app/shared/__mocks__/shared.module';
import { BookmarksComponent as TestedComponent } from './bookmarks.component';
import { STORE_FEATURE_KEY } from '../../constants';

@Component({
  template: '<router-outlet></router-outlet>',
})
class RootStubComponent {
  @ViewChild(RouterOutlet)
  routerOutlet!: RouterOutlet;
}

describe('BookmarksComponent', () => {
  const defaultRouterPath = 'bookmarks';
  const initialState = {
    [STORE_FEATURE_KEY]: {
      bookmarks: { bookmarksList: [] },
    },
  };

  let fixture: ComponentFixture<RootStubComponent>;
  let mainTemplate: HTMLElement;
  let router: Router;
  let store: MockStore;
  let location: Location;
  let ngZone: NgZone;
  let component: TestedComponent;

  const initComponent = () => {
    fixture = TestBed.createComponent(RootStubComponent);
    fixture.detectChanges();

    component = fixture.componentInstance.routerOutlet
      .component as TestedComponent;
    fixture.detectChanges();
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          { path: defaultRouterPath, component: TestedComponent },
          { path: `${defaultRouterPath}/add`, component: RouterStubComponent },
          {
            path: `${defaultRouterPath}/:id`,
            component: RouterStubComponent,
          },
          { path: '**', redirectTo: `/${defaultRouterPath}` },
        ]),
        SharedModule,
        FormsModule,
        OrderModule,
      ],
      declarations: [RootStubComponent, TestedComponent],
      providers: [provideMockStore({ initialState })],
    }).compileComponents();

    router = TestBed.inject(Router);
    store = TestBed.inject(MockStore);
    location = TestBed.inject(Location);
    ngZone = TestBed.inject(NgZone);

    ngZone.run(() => {
      router.initialNavigation();
    });
  });

  beforeEach(() => {
    initComponent();
    mainTemplate = fixture.debugElement.nativeElement;
  });

  describe('creating an instance', () => {
    it('should create an instance of BookmarksComponent class', () => {
      expect(component).toBeTruthy();
      expect(component instanceof TestedComponent).toBe(true);
    });
  });

  describe('checking a template', () => {
    it('should show Dashboard title', () => {
      fixture.detectChanges();

      const testedComponent = mainTemplate.querySelector('h2');
      expect(testedComponent).toBeTruthy();
      expect(testedComponent?.textContent).toBe('Bookmarks');
    });

    it('should show button Show all', () => {
      fixture.detectChanges();

      const testedComponent = mainTemplate.querySelector('div a');
      expect(testedComponent).toBeTruthy();
      expect(testedComponent?.textContent).toBe('Add');
    });

    it('should inform that a bookmarks list is empty', () => {
      fixture.detectChanges();

      const testedComponent = mainTemplate.querySelector('mat-card');
      expect(testedComponent).toBeTruthy();
      expect(testedComponent?.textContent).toBe(
        'Bookmarks list is empty found.',
      );
    });
  });

  describe('button Add', () => {
    const redirectRouter = 'bookmarks/add';
    it('should redirect to add bookmark page by click', () => {
      fixture.detectChanges();
      expect(location.path()).toBe(`/${defaultRouterPath}`);

      const testedButton: HTMLElement | null = mainTemplate.querySelector(
        'div a',
      );
      testedButton?.click();
      fixture.detectChanges();

      expect(location.path()).toBe(`/${redirectRouter}`);
    });
  });

  describe('bookmarks list', () => {
    const mockBookmarksData = [
      {
        id: 'qwe1',
        name: 'Z Test name 1',
        url: 'url1.com',
        group: 'test',
      },
      {
        id: 'qwe1',
        name: 'Test name 1',
        url: 'url1.com',
        group: 'test',
      },
      {
        id: 'qwe2',
        name: 'A Test name 2',
        url: 'url2.com',
        group: 'test',
      },
      {
        id: 'qwe3',
        name: 'Test name',
        url: 'url.com',
        group: 'aTest1',
      },
      {
        id: 'qwe6',
        name: 'Test name 2',
        url: 'url2.com',
        group: 'zTest2',
      },
    ];

    beforeEach(() => {
      store.setState({
        [STORE_FEATURE_KEY]: {
          bookmarks: { bookmarksList: mockBookmarksData },
        },
      });
      fixture.detectChanges();
    });

    it('should show grouped and sorted bookmarks list', () => {
      const bookmarksList = mainTemplate.querySelectorAll(
        'mat-expansion-panel',
      );

      expect(bookmarksList.length).toBe(3);
      const groupName1 = bookmarksList[0].querySelector(
        'mat-expansion-panel-header',
      );
      expect(groupName1?.textContent).toBe('aTest1');

      const groupName2 = bookmarksList[1].querySelector(
        'mat-expansion-panel-header',
      );
      expect(groupName2?.textContent).toBe('test');

      const groupName3 = bookmarksList[2].querySelector(
        'mat-expansion-panel-header',
      );
      expect(groupName3?.textContent).toBe('zTest2');

      const groupList = bookmarksList[1].querySelectorAll('mat-list-option');
      expect(groupList?.length).toBe(3);

      expect(groupList[0]?.textContent?.trim()).toBe('A Test name 2');
      expect(groupList[1]?.textContent?.trim()).toBe('Test name 1');
      expect(groupList[2]?.textContent?.trim()).toBe('Z Test name 1');
    });

    it('should redirect to bookmark details page by clicking on a bookmark', fakeAsync(() => {
      expect(location.path()).toBe(`/${defaultRouterPath}`);

      const selectedBookmarks = mainTemplate.querySelector(
        'mat-expansion-panel:nth-child(1) mat-list-option',
      ) as HTMLElement;

      selectedBookmarks?.click();
      fixture.detectChanges();
      tick();

      expect(location.path()).toBe(`/${defaultRouterPath}/qwe3`);
    }));
  });
});
