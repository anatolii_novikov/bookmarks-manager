import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';

import { AppState } from '@app/store';
import { Bookmark } from '@api/models/bookmark.model';
import { selectBookmarksList } from '../../state/bookmarks/bookmarks.selectors';

@Component({
  templateUrl: './bookmarks.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BookmarksComponent {
  public groupedBookmarks$: Observable<
    Map<string, Bookmark[]> | false
  > = this.store.pipe(
    select(selectBookmarksList),
    filter((bookmarks): bookmarks is Bookmark[] => bookmarks !== null),
    map((bookmarks: Bookmark[]) =>
      bookmarks.length === 0
        ? false
        : bookmarks.reduce<Map<string, Bookmark[]>>(
            (previousValue, currentValue) => {
              const outResult = new Map(previousValue);
              if (!outResult.has(currentValue.group)) {
                outResult.set(currentValue.group, []);
              }
              outResult.get(currentValue.group)?.push(currentValue);
              return outResult;
            },
            new Map(),
          ),
    ),
  );

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  showBookmark(id: string) {
    this.router.navigate([id], { relativeTo: this.route });
  }
}
