jest.mock(
  '@app/shared/components/confirmation-delete-dialog/confirmation-delete-dialog',
);

import {
  TestBed,
  ComponentFixture,
  fakeAsync,
  tick,
} from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Location } from '@angular/common';
import { Injectable, NgZone } from '@angular/core';
import { cold, Scheduler } from 'jest-marbles';
import { MatDialog } from '@angular/material/dialog';
import { from, of, Subject } from 'rxjs';

import {
  RouterStubComponent,
  SharedModule,
} from '@app/shared/__mocks__/shared.module';
import { TaskLoadedSuccessfully, TaskLoadingStarted } from '@app/store';
import SpyInstance = jest.SpyInstance;
import { SelectedBookmarkComponent as TestedComponent } from './selected-bookmark.component';
import { STORE_FEATURE_KEY } from '../../constants';
import { BookmarkDeleted } from '../../state/bookmarks/bookmarks.actions';

const afterClosedSubject = new Subject();

const dialogMock = {
  afterClosed: () => afterClosedSubject.asObservable(),
};

@Injectable()
class MatDialogStub {
  open() {
    return dialogMock;
  }
}

describe('SelectedBookmarkComponent', () => {
  const defaultRouterPath = 'bookmarks';
  const initialState = {
    [STORE_FEATURE_KEY]: {
      bookmarks: { bookmarksList: [] },
      selectedBookmark: false,
    },
  };

  let router: Router;
  let fixture: ComponentFixture<TestedComponent>;
  let mainTemplate: HTMLElement;
  let store: MockStore;
  let location: Location;
  let ngZone: NgZone;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          { path: defaultRouterPath, component: RouterStubComponent },
          { path: `${defaultRouterPath}/add`, component: RouterStubComponent },
          {
            path: `${defaultRouterPath}/:id`,
            component: TestedComponent,
          },
          { path: '**', redirectTo: `/${defaultRouterPath}/qwe` },
        ]),
        SharedModule,
      ],
      declarations: [TestedComponent],
      providers: [
        provideMockStore({ initialState }),
        { provide: MatDialog, useClass: MatDialogStub },
      ],
    }).compileComponents();

    router = TestBed.inject(Router);
    store = TestBed.inject(MockStore);
    location = TestBed.inject(Location);
    ngZone = TestBed.inject(NgZone);

    fixture = TestBed.createComponent(TestedComponent);
    mainTemplate = fixture.debugElement.nativeElement;

    ngZone.run(() => {
      router.initialNavigation();
    });
  });

  describe('creating an instance', () => {
    it('should create an instance of SelectedBookmarkComponent class', () => {
      const app = fixture.debugElement.componentInstance;
      expect(app).toBeTruthy();
      expect(app instanceof TestedComponent).toBe(true);
    });
  });

  describe('checking a template', () => {
    it('should show Dashboard title', () => {
      fixture.detectChanges();

      const testedComponent = mainTemplate.querySelector('h2');
      expect(testedComponent).toBeTruthy();
      expect(testedComponent?.textContent).toBe('Bookmarks');
    });

    it('should show button Show all', () => {
      fixture.detectChanges();

      const testedComponent = mainTemplate.querySelector('div a');
      expect(testedComponent).toBeTruthy();
      expect(testedComponent?.textContent).toBe('Show all');
    });

    it('should hide button Delete if a bookmark does not exist', () => {
      fixture.detectChanges();

      const testedComponent = mainTemplate.querySelector('div button');
      expect(testedComponent).toBeFalsy();
    });

    it('should inform that a bookmark does not exist', () => {
      fixture.detectChanges();

      const testedComponent = mainTemplate.querySelector('mat-card');
      expect(testedComponent).toBeTruthy();
      expect(testedComponent?.textContent?.trim()).toBe(
        'Bookmark was not found.',
      );
    });
  });

  describe('bookmark details', () => {
    const selectedBookmarkMock = {
      id: 'qwe6',
      name: 'Test name 2',
      url: 'url2.com',
      group: 'zTest2',
    };

    beforeEach(() => {
      store.setState({
        [STORE_FEATURE_KEY]: {
          bookmarks: {
            bookmarksList: [selectedBookmarkMock],
            selectedBookmark: selectedBookmarkMock,
          },
        },
      });
      fixture.detectChanges();
    });

    it('should show button Delete', () => {
      const testedComponent = mainTemplate.querySelector('div button');
      expect(testedComponent).toBeTruthy();
      expect(testedComponent?.textContent?.trim()).toBe('Delete');
    });

    it('should show bookmark details', () => {
      const bookmarkDetails = mainTemplate.querySelector(
        'mat-card',
      ) as HTMLElement;
      expect(bookmarkDetails).toBeTruthy();

      const bookmarkTitle = bookmarkDetails.querySelector('mat-card-title');
      expect(bookmarkTitle?.textContent?.trim()).toBe(
        selectedBookmarkMock.name,
      );

      const bookmarkGroup = bookmarkDetails.querySelector('mat-card-subtitle');
      expect(bookmarkGroup?.textContent?.trim()).toBe(
        `Group: ${selectedBookmarkMock.group}`,
      );

      const bookmarkUrl = bookmarkDetails.querySelector('mat-card-content');
      expect(bookmarkUrl?.textContent?.trim()).toBe(selectedBookmarkMock.url);
    });

    describe('delete workflow', () => {
      let deleteButton: HTMLButtonElement;
      let dispatchSpy: SpyInstance;

      beforeEach(() => {
        deleteButton = mainTemplate.querySelector(
          'div button',
        ) as HTMLButtonElement;

        dispatchSpy = jest.spyOn(store, 'dispatch');
        dispatchSpy.mockImplementation(jest.fn());
      });

      afterEach(() => {
        dispatchSpy.mockRestore();
      });

      it('should delete a current bookmark', () => {
        deleteButton.click();

        afterClosedSubject.next(true);
        fixture.detectChanges();

        expect(dispatchSpy).toBeCalledWith(
          new BookmarkDeleted(selectedBookmarkMock.id),
        );
      });

      it('should show a loader', () => {
        deleteButton.click();

        afterClosedSubject.next(true);
        fixture.detectChanges();

        expect(dispatchSpy).toBeCalledWith(expect.any(TaskLoadingStarted));
        expect(dispatchSpy).not.toBeCalledWith(
          expect.any(TaskLoadedSuccessfully),
        );
      });

      it('should redirect to bookmarks after a bookmark deleted successfully', fakeAsync(() => {
        expect(location.path()).toBe('/bookmarks/qwe');

        deleteButton.click();
        fixture.detectChanges();

        afterClosedSubject.next(true);
        fixture.detectChanges();

        store.setState({
          [STORE_FEATURE_KEY]: {
            bookmarks: {
              bookmarksList: [],
              selectedBookmark: null,
            },
          },
        });
        tick();

        expect(location.path()).toBe('/bookmarks');
      }));

      it('should unsubscribe from store observable', fakeAsync(() => {
        const bookmarkStream = cold('--d--', {
          d: null,
        });

        fixture.debugElement.componentInstance.bookmark$ = bookmarkStream;

        deleteButton.click();

        afterClosedSubject.next(true);
        Scheduler.get().flush();
        tick();

        expect(bookmarkStream).toHaveSubscriptions('^-!');
      }));
    });
  });
});
