import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

import {
  AppState,
  TaskLoadedSuccessfully,
  TaskLoadingStarted,
} from '@app/store';
import { Bookmark } from '@api/models/bookmark.model';
import { BookmarksManagerConfirmationDeleteDialogComponent } from '@app/shared/components/confirmation-delete-dialog/confirmation-delete-dialog';
import { selectSelectedBookmark } from '../../state/bookmarks/bookmarks.selectors';
import { BookmarkDeleted } from '../../state/bookmarks/bookmarks.actions';

@Component({
  templateUrl: './selected-bookmark.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectedBookmarkComponent {
  public bookmark$: Observable<Bookmark | null | false> = this.store.pipe(
    select(selectSelectedBookmark),
  );

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private dialog: MatDialog,
  ) {}

  confirmDelete(id: string) {
    const dialogRef = this.dialog.open(
      BookmarksManagerConfirmationDeleteDialogComponent,
    );
    const dialogSubscription = dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.deleteBookmark(id);
      }
      dialogSubscription.unsubscribe();
    });
  }

  deleteBookmark(id: string) {
    const checkSubscription = this.bookmark$.subscribe(bookmark => {
      if (!bookmark) {
        checkSubscription.unsubscribe();
        this.store.dispatch(new TaskLoadedSuccessfully());
        this.router.navigate(['/', 'bookmarks']);
      }
    });
    this.store.dispatch(new BookmarkDeleted(id));
    this.store.dispatch(new TaskLoadingStarted());
  }
}
