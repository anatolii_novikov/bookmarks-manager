import {
  TestBed,
  ComponentFixture,
  fakeAsync,
  tick,
} from '@angular/core/testing';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { provideMockActions } from '@ngrx/effects/testing';
import { hot, Scheduler } from 'jest-marbles';
import { OrderModule } from 'ngx-order-pipe';

import {
  RouterStubComponent,
  SharedModule,
} from '@app/shared/__mocks__/shared.module';
import { TaskLoadedSuccessfully, TaskLoadingStarted } from '@app/store';
import SpyInstance = jest.SpyInstance;
import { AddBookmarkComponent as TestedComponent } from './add-bookmark.component';
import {
  BookmarkAdded,
  BookmarkAddedSuccessfully,
} from '../../state/bookmarks/bookmarks.actions';
import { STORE_FEATURE_KEY } from '../../constants';

describe('AddBookmarkComponent', () => {
  const defaultRouterPath = '';
  const redirectRouter = 'bookmarks';
  const initialState = {
    [STORE_FEATURE_KEY]: {
      bookmarks: {
        bookmarksList: [
          {
            id: 'qwe1',
            name: 'Z Test name 1',
            url: 'url1.com',
            group: 'test',
          },
          {
            id: 'qwe1',
            name: 'Test name 1',
            url: 'url1.com',
            group: 'test',
          },
          {
            id: 'qwe3',
            name: 'Test name',
            url: 'url.com',
            group: 'aTest1',
          },
          {
            id: 'qwe6',
            name: 'Test name 2',
            url: 'url2.com',
            group: 'zTest2',
          },
        ],
      },
    },
  };

  let router: Router;
  let fixture: ComponentFixture<TestedComponent>;
  let mainTemplate: HTMLElement;
  let store: MockStore;
  let location: Location;
  let ngZone: NgZone;
  let actions$: any;

  beforeEach(async () => {
    actions$ = hot('--a--', { a: new BookmarkAddedSuccessfully() });

    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          { path: redirectRouter, component: RouterStubComponent },
          { path: defaultRouterPath, component: TestedComponent },
          { path: '**', redirectTo: defaultRouterPath },
        ]),
        SharedModule,
        FormsModule,
        OrderModule,
      ],
      declarations: [TestedComponent],
      providers: [
        provideMockStore({ initialState }),
        provideMockActions(() => actions$),
      ],
    }).compileComponents();

    router = TestBed.inject(Router);
    store = TestBed.inject(MockStore);
    location = TestBed.inject(Location);
    ngZone = TestBed.inject(NgZone);

    fixture = TestBed.createComponent(TestedComponent);
    mainTemplate = fixture.debugElement.nativeElement;

    ngZone.run(() => {
      router.initialNavigation();
    });
  });

  describe('creating an instance', () => {
    it('should create an instance of AddBookmarkComponent class', () => {
      const app = fixture.debugElement.componentInstance;
      expect(app).toBeTruthy();
      expect(app instanceof TestedComponent).toBe(true);
    });
  });

  describe('checking a template', () => {
    it('should show Dashboard title', () => {
      fixture.detectChanges();

      const testedComponent = mainTemplate.querySelector('h2');
      expect(testedComponent).toBeTruthy();
      expect(testedComponent?.textContent).toBe('Add New Bookmark');
    });

    it('should show button Show all', () => {
      fixture.detectChanges();

      const testedComponent = mainTemplate.querySelector('div a');
      expect(testedComponent).toBeTruthy();
      expect(testedComponent?.textContent).toBe('Show all');
    });

    it('should show form for adding bookmark', () => {
      fixture.detectChanges();

      const testedComponent = mainTemplate.querySelector('form');
      expect(testedComponent).toBeTruthy();
    });
  });

  describe('button Show all', () => {
    it('should redirect to bookmarks page by click', () => {
      fixture.detectChanges();
      expect(location.path()).toBe('/');

      const testedButton: HTMLElement | null = mainTemplate.querySelector(
        'div a',
      );
      testedButton?.click();
      fixture.detectChanges();

      expect(location.path()).toBe('/bookmarks');
    });
  });

  describe('adding form', () => {
    let formElement: HTMLFormElement | null;

    beforeEach(() => {
      fixture.detectChanges();

      formElement = mainTemplate.querySelector('form');
    });

    describe('checking form elements', () => {
      it('should contain Name form input', () => {
        const formFieldComponent = formElement?.querySelector(
          'mat-form-field:nth-child(1)',
        );
        expect(formFieldComponent).toBeTruthy();

        const inputFieldComponent = formFieldComponent?.querySelector('input');
        expect(inputFieldComponent).toBeTruthy();

        const labelFieldComponent = formFieldComponent?.querySelector(
          'mat-label',
        );
        expect(labelFieldComponent).toBeTruthy();
        expect(labelFieldComponent?.textContent).toBe('Name');
      });

      it('should contain Group form input', () => {
        const formFieldComponent = formElement?.querySelector(
          'mat-form-field:nth-child(2)',
        );
        expect(formFieldComponent).toBeTruthy();

        const inputFieldComponent = formFieldComponent?.querySelector('input');
        expect(inputFieldComponent).toBeTruthy();

        const labelFieldComponent = formFieldComponent?.querySelector(
          'mat-label',
        );
        expect(labelFieldComponent).toBeTruthy();
        expect(labelFieldComponent?.textContent).toBe('Group');
      });

      it('should contain URL form input', () => {
        const formFieldComponent = formElement?.querySelector(
          'mat-form-field:nth-child(3)',
        );
        expect(formFieldComponent).toBeTruthy();

        const inputFieldComponent = formFieldComponent?.querySelector('input');
        expect(inputFieldComponent).toBeTruthy();

        const labelFieldComponent = formFieldComponent?.querySelector(
          'mat-label',
        );
        expect(labelFieldComponent).toBeTruthy();
        expect(labelFieldComponent?.textContent).toBe('URL');
      });

      it('should contain submit button', () => {
        const buttonComponent = formElement?.querySelector('div button');
        expect(buttonComponent).toBeTruthy();
        expect(buttonComponent?.textContent?.trim()).toBe('Add');
      });
    });

    describe('group autocomplete', () => {
      it('should show all exist sorted group by default', () => {
        const groupElements = formElement?.querySelectorAll(
          'mat-form-field:nth-child(2) mat-autocomplete mat-option',
        );
        expect(groupElements?.length).toBe(3);
        expect(groupElements[0].textContent.trim()).toBe('aTest1');
        expect(groupElements[1].textContent.trim()).toBe('test');
        expect(groupElements[2].textContent.trim()).toBe('zTest2');
      });

      it('should show filtered sorted group', () => {
        const groupElements = formElement?.querySelectorAll(
          'mat-form-field:nth-child(2) mat-autocomplete mat-option',
        );
        expect(groupElements?.length).toBe(3);

        const groupInputField = formElement?.querySelector(
          'mat-form-field:nth-child(2) input',
        ) as HTMLInputElement;
        groupInputField.value = 'aT';
        groupInputField.dispatchEvent(new Event('input'));
        fixture.detectChanges();

        const filteredGroupElements = formElement?.querySelectorAll(
          'mat-form-field:nth-child(2) mat-autocomplete mat-option',
        );

        expect(filteredGroupElements?.length).toBe(1);
        expect(filteredGroupElements[0].textContent.trim()).toBe('aTest1');
      });
    });

    describe('validation workflow', () => {
      describe('default untouched form', () => {
        it('should hide validation error for Name input', () => {
          const errorFieldComponent = formElement?.querySelector(
            'mat-form-field:nth-child(1) mat-error',
          ) as HTMLElement;

          expect(errorFieldComponent).toBeTruthy();
          expect(errorFieldComponent.hidden).toBe(true);
        });

        it('should hide validation error for Group input', () => {
          const errorFieldComponent = formElement?.querySelector(
            'mat-form-field:nth-child(2) mat-error',
          ) as HTMLElement;

          expect(errorFieldComponent).toBeTruthy();
          expect(errorFieldComponent.hidden).toBe(true);
        });

        it('should hide validation error for URL input', () => {
          const errorFieldComponent = formElement?.querySelector(
            'mat-form-field:nth-child(3) mat-error',
          ) as HTMLElement;

          expect(errorFieldComponent).toBeFalsy();
        });
      });

      describe('checking entered data', () => {
        let nameInputField: HTMLInputElement;
        let groupInputField: HTMLInputElement;
        let urlInputField: HTMLInputElement;
        let submitButton: HTMLButtonElement;

        beforeEach(() => {
          fixture.detectChanges();
          nameInputField = formElement?.querySelector(
            'mat-form-field:nth-child(1) input',
          ) as HTMLInputElement;
          groupInputField = formElement?.querySelector(
            'mat-form-field:nth-child(2) input',
          ) as HTMLInputElement;
          urlInputField = formElement?.querySelector(
            'mat-form-field:nth-child(3) input',
          ) as HTMLInputElement;

          submitButton = formElement?.querySelector(
            'div button',
          ) as HTMLButtonElement;

          nameInputField.value = 'someValue';
          groupInputField.value = 'someValue';
          urlInputField.value = 'someValue';

          nameInputField.dispatchEvent(new Event('input'));
          groupInputField.dispatchEvent(new Event('input'));
          urlInputField.dispatchEvent(new Event('input'));

          fixture.detectChanges();
        });

        it('should show error if Name input is empty', () => {
          nameInputField.value = '';
          nameInputField.dispatchEvent(new Event('input'));
          fixture.detectChanges();

          const errorFieldComponent = formElement?.querySelector(
            'mat-form-field:nth-child(1) mat-error',
          ) as HTMLElement;

          expect(errorFieldComponent.hidden).toBe(false);
          expect(errorFieldComponent?.textContent?.trim()).toBe(
            'Name is required',
          );
          expect(submitButton.disabled).toBe(true);
        });

        it('should show error if Group input is empty', () => {
          groupInputField.value = '';
          groupInputField.dispatchEvent(new Event('input'));
          fixture.detectChanges();

          const errorFieldComponent = formElement?.querySelector(
            'mat-form-field:nth-child(2) mat-error',
          ) as HTMLElement;

          expect(errorFieldComponent.hidden).toBe(false);
          expect(errorFieldComponent?.textContent?.trim()).toBe(
            'Group is required',
          );
          expect(submitButton.disabled).toBe(true);
        });

        it('should show error if URL input is empty', () => {
          urlInputField.value = '';
          urlInputField.dispatchEvent(new Event('input'));
          fixture.detectChanges();

          const errorFieldComponent = formElement?.querySelector(
            'mat-form-field:nth-child(3) mat-error',
          ) as HTMLElement;

          expect(errorFieldComponent.hidden).toBe(false);
          expect(errorFieldComponent?.textContent?.trim()).toBe(
            'URL is required',
          );
          expect(submitButton.disabled).toBe(true);
        });

        it('should show error if URL is incorrect', () => {
          urlInputField.value = 'test';
          urlInputField.dispatchEvent(new Event('input'));
          fixture.detectChanges();

          const errorFieldComponent = formElement?.querySelector(
            'mat-form-field:nth-child(3) mat-error',
          ) as HTMLElement;

          expect(errorFieldComponent.hidden).toBe(false);
          expect(errorFieldComponent?.textContent?.trim()).toBe(
            'Incorrect URL format',
          );
          expect(submitButton.disabled).toBe(true);
        });
      });
    });

    describe('submit workflow', () => {
      const testNameValue = 'Test Name';
      const testGroupValue = 'Test Group';
      const testUrlValue = 'http://www.test';

      let submitButton: HTMLButtonElement;
      let dispatchSpy: SpyInstance;

      beforeEach(() => {
        fixture.detectChanges();
        const nameInputField = formElement?.querySelector(
          'mat-form-field:nth-child(1) input',
        ) as HTMLInputElement;
        const groupInputField = formElement?.querySelector(
          'mat-form-field:nth-child(2) input',
        ) as HTMLInputElement;
        const urlInputField = formElement?.querySelector(
          'mat-form-field:nth-child(3) input',
        ) as HTMLInputElement;

        submitButton = formElement?.querySelector(
          'div button',
        ) as HTMLButtonElement;

        nameInputField.value = testNameValue;
        groupInputField.value = testGroupValue;
        urlInputField.value = testUrlValue;

        nameInputField.dispatchEvent(new Event('input'));
        groupInputField.dispatchEvent(new Event('input'));
        urlInputField.dispatchEvent(new Event('input'));

        nameInputField.dispatchEvent(new Event('input'));

        fixture.detectChanges();

        dispatchSpy = jest.spyOn(store, 'dispatch');
        dispatchSpy.mockImplementation(jest.fn());
      });

      afterEach(() => {
        dispatchSpy.mockRestore();
      });

      it('should enable the submit button', () => {
        expect(submitButton.disabled).toBe(false);
      });

      it('should submit form with data', () => {
        submitButton.click();

        expect(dispatchSpy).toBeCalledWith(
          new BookmarkAdded({
            group: testGroupValue,
            name: testNameValue,
            url: testUrlValue,
          }),
        );
      });

      it('should show a loader', () => {
        submitButton.click();

        expect(dispatchSpy).toBeCalledWith(expect.any(TaskLoadingStarted));
        expect(dispatchSpy).not.toBeCalledWith(
          expect.any(TaskLoadedSuccessfully),
        );
      });

      it('should redirect to bookmarks after a bookmark stored successfully', fakeAsync(() => {
        expect(location.path()).toBe('/');

        submitButton.click();
        fixture.detectChanges();

        Scheduler.get().flush();
        tick();

        expect(location.path()).toBe('/bookmarks');
      }));

      it('should unsubscribe from action observable', fakeAsync(() => {
        submitButton.click();
        fixture.detectChanges();

        Scheduler.get().flush();
        tick();

        expect(actions$).toHaveSubscriptions('^-!');
      }));
    });
  });
});
