import { ChangeDetectionStrategy, Component } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { Actions, ofType } from '@ngrx/effects';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';

import {
  AppState,
  TaskLoadedSuccessfully,
  TaskLoadingStarted,
} from '@app/store';
import { Bookmark } from '@api/models';
import {
  BookmarkAdded,
  BookmarkAddedSuccessfully,
  BookmarksActionsEnum,
} from '../../state/bookmarks/bookmarks.actions';
import { selectBookmarksList } from '../../state/bookmarks/bookmarks.selectors';

@Component({
  templateUrl: './add-bookmark.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddBookmarkComponent {
  public bookmarkName = '';

  public bookmarkGroup = '';

  public bookmarkUrl = '';

  public bookmarkGroupChanged$ = new BehaviorSubject(this.bookmarkGroup);

  public filteredGroups$: Observable<string[]> = this.store.pipe(
    select(selectBookmarksList),
    filter((bookmarks): bookmarks is Bookmark[] => bookmarks !== null),
    map((bookmarks: Bookmark[]) =>
      Array.from(new Set(bookmarks.map(bookmark => bookmark.group))),
    ),
    switchMap((groups: string[]) =>
      this.bookmarkGroupChanged$.pipe(
        map(value =>
          groups.filter(group =>
            group.toLowerCase().includes(value.toLowerCase()),
          ),
        ),
      ),
    ),
  );

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private actions$: Actions,
  ) {}

  addBookmark() {
    const checkSubscription = this.actions$
      .pipe(
        ofType<BookmarkAddedSuccessfully>(
          BookmarksActionsEnum.bookmarkAddedSuccessfully,
        ),
      )
      .subscribe(() => {
        checkSubscription.unsubscribe();
        this.store.dispatch(new TaskLoadedSuccessfully());
        this.router.navigate(['/', 'bookmarks']);
      });
    this.store.dispatch(
      new BookmarkAdded({
        group: this.bookmarkGroup,
        name: this.bookmarkName,
        url: this.bookmarkUrl,
      }),
    );
    this.store.dispatch(new TaskLoadingStarted());
  }
}
