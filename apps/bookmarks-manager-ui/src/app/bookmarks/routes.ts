import { Routes } from '@angular/router';
import { BookmarkResolver } from './resolvers/bookmark.resolver';
import { BookmarksListResolver } from './resolvers/bookmarks-list.resolver';
import { SelectedBookmarkComponent } from './components/selected-bookmark/selected-bookmark.component';
import { AddBookmarkComponent } from './components/add-bookmark/add-bookmark.component';
import { BookmarksComponent } from './components/bookmarks/bookmarks.component';

export const routes: Routes = [
  {
    path: '',
    component: BookmarksComponent,
    resolve: {
      bookmarksList: BookmarksListResolver,
    },
  },
  {
    path: 'add',
    component: AddBookmarkComponent,
    resolve: {
      bookmarksList: BookmarksListResolver,
    },
  },
  {
    path: ':id',
    component: SelectedBookmarkComponent,
    resolve: {
      bookmark: BookmarkResolver,
    },
  },
];
