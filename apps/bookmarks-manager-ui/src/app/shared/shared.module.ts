import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';

import { BookmarksManagerConfirmationDeleteDialogComponent } from './components/confirmation-delete-dialog/confirmation-delete-dialog';

@NgModule({
  imports: [MatDialogModule, MatButtonModule],
  exports: [CommonModule, FlexLayoutModule, MatDialogModule, MatButtonModule],
  declarations: [BookmarksManagerConfirmationDeleteDialogComponent],
})
export class SharedModule {}
