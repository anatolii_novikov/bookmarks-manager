import { Component, NgModule, Input, Directive } from '@angular/core';

@Component({
  template: '<ng-content></ng-content>',
})
export class RouterStubComponent {}

@Component({
  selector: 'mat-label',
  template: '<ng-content></ng-content>',
})
class MatLabelStubComponent {}

@Component({
  selector: 'mat-error',
  template: '<ng-content></ng-content>',
})
class MatErrorStubComponent {}

@Component({
  selector: 'mat-form-field',
  template: '<ng-content></ng-content>',
})
class MatFormFieldStubComponent {}

@Component({
  selector: 'mat-card-content',
  template: '<ng-content></ng-content>',
})
class MatCardContentStubComponent {}

@Component({
  selector: 'mat-card',
  template: '<ng-content></ng-content>',
})
class MatCardStubComponent {}

@Component({
  selector: 'mat-panel-title',
  template: '<ng-content></ng-content>',
})
class MatPanelTitletubComponent {}

@Component({
  selector: 'mat-list-option',
  template: '<ng-content></ng-content>',
})
class MatListOptionComponent {}

@Component({
  selector: 'mat-expansion-panel-header',
  template: '<ng-content></ng-content>',
})
class MatExpansionPanelHeaderComponent {}

@Component({
  selector: 'mat-expansion-panel',
  template: '<ng-content></ng-content>',
})
class MatExpansionPanelComponent {}

@Component({
  selector: 'mat-selection-list',
  template: '<ng-content></ng-content>',
})
class MatSelectionListComponent {
  @Input() multiple!: string;
}

@Component({
  selector: 'mat-card-title',
  template: '<ng-content></ng-content>',
})
class MatCardTitleComponent {}

@Component({
  selector: 'mat-card-subtitle',
  template: '<ng-content></ng-content>',
})
class MatCardSubtitleComponent {}

@Component({
  selector: 'mat-card-header',
  template: '<ng-content></ng-content>',
})
class MatCardHeaderComponent {}

@Component({
  selector: 'bookmarks-manager-footer',
  template: '<ng-content></ng-content>',
})
class BookmarksManagerFooterComponent {}

@Component({
  selector: 'bookmarks-manager-header',
  template: '<ng-content></ng-content>',
})
class BookmarksManagerHeaderComponent {}

@Component({
  selector: 'mat-autocomplete',
  template: '<ng-content></ng-content>',
  exportAs: 'matAutocomplete',
})
class MatAutocompleteComponent {}

@Directive({
  selector: `input[matAutocomplete], textarea[matAutocomplete]`,
})
export class MatAutocompleteMockTrigger {
  // eslint-disable-next-line @angular-eslint/no-input-rename
  @Input('matAutocomplete') autocomplete: any;
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mat-option',
  template: '<ng-content></ng-content>',
})
class MatOptionStubComponent {
  @Input() value!: string;
}

@NgModule({
  declarations: [
    RouterStubComponent,
    MatLabelStubComponent,
    MatErrorStubComponent,
    MatFormFieldStubComponent,
    MatCardContentStubComponent,
    MatCardStubComponent,
    MatPanelTitletubComponent,
    MatExpansionPanelHeaderComponent,
    MatSelectionListComponent,
    MatListOptionComponent,
    MatExpansionPanelComponent,
    MatCardTitleComponent,
    MatCardSubtitleComponent,
    MatCardHeaderComponent,
    BookmarksManagerFooterComponent,
    BookmarksManagerHeaderComponent,
    MatAutocompleteComponent,
    MatAutocompleteMockTrigger,
    MatOptionStubComponent,
  ],
  exports: [
    RouterStubComponent,
    MatLabelStubComponent,
    MatErrorStubComponent,
    MatFormFieldStubComponent,
    MatCardContentStubComponent,
    MatCardStubComponent,
    MatPanelTitletubComponent,
    MatExpansionPanelHeaderComponent,
    MatSelectionListComponent,
    MatListOptionComponent,
    MatExpansionPanelComponent,
    MatCardTitleComponent,
    MatCardSubtitleComponent,
    MatCardHeaderComponent,
    BookmarksManagerFooterComponent,
    BookmarksManagerHeaderComponent,
    MatAutocompleteComponent,
    MatAutocompleteMockTrigger,
    MatOptionStubComponent,
  ],
})
export class SharedModule {}
