import { Component } from '@angular/core';

@Component({
  selector: 'bookmarks-manager-confirmation-delete-dialog',
  templateUrl: 'confirmation-delete-dialog.html',
})
export class BookmarksManagerConfirmationDeleteDialogComponent {}
